#!/usr/bin/python3
import sys
import subprocess

def checkCommitMsg(filename):
	comand = "cat " + filename + " | sed '/^\s*#/d;/^\s*$/d' | wc -l"
	size = int(subprocess.check_output(comand, shell=True).decode('ascii').strip())
	
	if size >= 2:
		return True
	else:
		return False

if __name__ == '__main__':
    commitMsg = sys.argv[1]
    if not checkCommitMsg(commitMsg):
        print('############\nThe commit message does not have enough lines\n############')
        exit(1)
