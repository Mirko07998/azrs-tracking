#!/usr/bin/python3
import sys
import subprocess

if __name__ == '__main__':

		branch = subprocess.check_output("git rev-parse --abbrev-ref HEAD", shell=True).decode('ascii').strip()
		
		if branch == "master":
			print("Ne mozete da pushujete direktno na master granu")
			sys.exit(1)
		else:
			sys.exit(0)
	
